import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
 @override
 Widget build(BuildContext context) {
   return MaterialApp(
     home: Scaffold(
       backgroundColor: Colors.grey[200],
       body: SignUpScreen(),
     ),
   );
 }
}

class SignUpScreen extends StatelessWidget {
 @override
 Widget build(BuildContext context) {
   return Center(
     child: SizedBox(
       width: 400,
       child: Card(
         child: SignUpForm(),
       ),
     ),
   );
 }
}

class SignUpForm extends StatefulWidget {
 @override
 _SignUpFormState createState() => _SignUpFormState();
}

class _SignUpFormState extends State<SignUpForm>
   with SingleTickerProviderStateMixin {
 // STEP 3: Add an AnimationController and add the
 // AnimatedBuilder with a LinearProgressIndicator to the Column

 @override
 void initState() {
   super.initState();
 }


 @override
 Widget build(BuildContext context) {
   return Column(
     mainAxisSize: MainAxisSize.min,
     children: [
       SignUpFormBody(
         onProgressChanged: (progress) {
           // STEP 2: Create a _formCompleted field on this class
           // and set it here when the progress is 1
         },
       ),
       Container(
         height: 40,
         width: double.infinity,
         margin: EdgeInsets.all(12),
         child: FlatButton(
           color: Colors.blue,
           textColor: Colors.white,
           // STEP 1: Add a callback here and navigate to
           // the welcome screen when the button is tapped.
           onPressed: null,
           child: Text('Sign up'),
         ),
       ),
     ],
   );
 }
}

class SignUpFormBody extends StatefulWidget {
 final ValueChanged<double> onProgressChanged;

 SignUpFormBody({
   @required this.onProgressChanged,
 }) : assert(onProgressChanged != null);

 @override
 _SignUpFormBodyState createState() => _SignUpFormBodyState();
}

class _SignUpFormBodyState extends State<SignUpFormBody> {
  static const EdgeInsets padding = EdgeInsets.all(8);
  final TextEditingController emailController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  final TextEditingController phoneController = TextEditingController();
  final TextEditingController whatsapp_phoneController =
      TextEditingController();
  final TextEditingController nameController = TextEditingController();
  final TextEditingController regIDController = TextEditingController();
  final TextEditingController academic_yearController = TextEditingController();
  final TextEditingController collegeController = TextEditingController();
  final TextEditingController courseController = TextEditingController();
  final TextEditingController branchController = TextEditingController();

  List<TextEditingController> get controllers => [
        emailController,
        passwordController,
        phoneController,
        whatsapp_phoneController,
        nameController,
        regIDController,
        academic_yearController,
        collegeController,
        courseController,
        branchController
      ];

  @override
  void initState() {
    super.initState();
    controllers.forEach((c) => c.addListener(() => _updateProgress()));
  }

  double get _formProgress {
    var progress = 0.0;
    for (var controller in controllers) {
      if (controller.value.text.isNotEmpty) {
        progress += 1 / controllers.length;
      }
    }
    return progress;
  }

  void _updateProgress() {
    widget.onProgressChanged(_formProgress);
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: padding,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: padding,
            child: Text('Sign up', style: Theme.of(context).textTheme.display1),
          ),
          SignUpField(
            hintText: 'Full Name',
            controller: nameController,
          ),
          SignUpField(
            hintText: 'E-mail address',
            controller: emailController,
          ),
          SignUpField(
            hintText: 'Password',
            controller: passwordController,
            obsecureText: true,
          ),
          SignUpField(
            hintText: 'Primary phone number',
            controller: phoneController,
          ),
          SignUpField(
            hintText: 'Whatsapp number',
            controller: whatsapp_phoneController,
            
          ),
          SignUpField(
            hintText: 'Registration ID',
            controller: regIDController,
          ),
          SignUpField(
            hintText: 'Academic year',
            controller: academic_yearController,
          ),
          SignUpField(
            hintText: 'College',
            controller: collegeController,
          ),
          SignUpField(
            hintText: 'Course',
            controller: courseController,
          ),
          SignUpField(
            hintText: 'Branch',
            controller: branchController,
          ),
        ],
      ),
    );
  }
}

class SignUpField extends StatelessWidget {
  final String hintText;
  final TextEditingController controller;
  var obsecureText;

  SignUpField({
    @required this.hintText,
    @required this.controller,
    this.obsecureText,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(8),
      child: TextFormField(
        obscureText: obsecureText,
        decoration: InputDecoration(hintText: hintText),
        controller: controller,
        
         
         ),
   );
 }
}
